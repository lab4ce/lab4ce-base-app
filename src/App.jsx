import { hot } from 'react-hot-loader/root';
import React from 'react';
import RootStore from './components/RootStore';
import STORE from './model/store';

import './App.scss';

import WelcomeMobx from './components/Welcome';

function App() {
  return (
    <main>
      <RootStore.Provider value={STORE}>
        <WelcomeMobx />
      </RootStore.Provider>
    </main>
  );
}

export default hot(App);
