import DemoModel from './DemoModel';

const STORE = {
  demo: new DemoModel(),
};

export default STORE;
