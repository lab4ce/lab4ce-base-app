import { makeAutoObservable, action } from 'mobx';

function simulateAxios(url) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      if (url) {
        resolve(`Got nice data from "${url}"`);
      } else {
        reject(new Error('Bad url!'));
      }
    }, 1500);
  });
}

class DemoModel {
  maxClicks;

  clicksCounter = 0;

  asyncData;

  constructor(maxClicks = 10) {
    makeAutoObservable(this);
    this.maxClicks = maxClicks;
  }

  get clicksLeft() {
    return this.maxClicks - this.clicksCounter;
  }

  incrementClick() {
    if (this.clicksCounter < this.maxClicks) {
      this.clicksCounter += 1;
    }
  }

  resetClick() {
    this.clicksCounter = 0;
  }

  fetchAsyncData(url) {
    simulateAxios(url)
      .then(action((data) => {
        this.asyncData = data;
      }))
      .catch(action((error) => {
        this.asyncData = `ERROR: ${error.message}`;
      }));
    this.asyncData = 'Fetching data...';
  }
}

export default DemoModel;
