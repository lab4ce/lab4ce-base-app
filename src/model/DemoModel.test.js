import DemoModel from './DemoModel';

describe('Simple DemoModel tests', () => {
  let testInstance;

  beforeEach(() => {
    testInstance = new DemoModel(30);
  });

  it('maxClicks properly set.', () => {
    expect(testInstance.maxClicks).toBe(30);
  });

  it('leftClicks decreases when clicks increases.', () => {
    expect(testInstance.clicksCounter).toBe(0);
    expect(testInstance.clicksLeft).toBe(30);
    testInstance.incrementClick();
    expect(testInstance.clicksCounter).toBe(1);
    expect(testInstance.clicksLeft).toBe(29);
    testInstance.incrementClick();
    expect(testInstance.clicksCounter).toBe(2);
    expect(testInstance.clicksLeft).toBe(28);
  });

  it('incrementClick does nothing when no click is left.', () => {
    while (testInstance.clicksLeft > 0) {
      testInstance.incrementClick();
    }
    expect(testInstance.clicksCounter).toBe(30);
    expect(testInstance.clicksLeft).toBe(0);
    testInstance.incrementClick();
    expect(testInstance.clicksCounter).toBe(30);
    expect(testInstance.clicksLeft).toBe(0);
  }, 150);

  it('resetClicks works properly.', () => {
    expect(testInstance.clicksCounter).toBe(0);
    expect(testInstance.clicksLeft).toBe(30);
    testInstance.incrementClick();
    testInstance.incrementClick();
    testInstance.incrementClick();
    expect(testInstance.clicksCounter).toBeGreaterThan(0);
    expect(testInstance.clicksLeft).toBeLessThan(30);
    testInstance.resetClick();
    expect(testInstance.clicksCounter).toBe(0);
    expect(testInstance.clicksLeft).toBe(30);
  });
});
