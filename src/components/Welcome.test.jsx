import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import RootStore from './RootStore';
import Welcome from './Welcome';

describe('Simple Welcome component tests', () => {
  let testStore;
  let welcomeComponent; // The Welcome tested component

  beforeAll(() => {
    testStore = {
      clicksCounter: 4,
      clicksLeft: 2,
    };
  });

  beforeEach(() => {
    welcomeComponent = shallow(
      <RootStore.Provider value={testStore}>
        <Welcome />
      </RootStore.Provider>,
    );
  });

  it('With the same props, the Welcome component dwells the same.', () => {
    expect(toJson(welcomeComponent)).toMatchSnapshot();
  });
});
