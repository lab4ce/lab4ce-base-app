import React, { useContext, useState } from 'react';
import { observer } from 'mobx-react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Jumbotron from 'react-bootstrap/Jumbotron';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import RootStore from './RootStore';

function WelcomeMobx() {
  const { demo } = useContext(RootStore);
  const [url, setUrl] = useState(null);

  return (
    <Container>
      <Row>
        <Col xs={12} md={6} lg={8} xl={9}>
          <Jumbotron>
            <h1 className="text-primary">Hello, Lab4CE Users!</h1>
            <p>This is a little demo component.</p>
            <ul>
              <li>
                You clicked
                {' '}
                {demo.clicksCounter}
                {' '}
                times.
              </li>
              <li>
                You have
                {' '}
                {demo.clicksLeft}
                {' '}
                click
                {demo.clicksLeft > 1 ? 's' : ''}
                {' '}
                left.
              </li>
            </ul>
            <p>
              <Button
                variant="info"
                disabled={demo.clicksLeft === 0}
                onClick={() => demo.incrementClick()}
              >
                Do a click!
              </Button>
              {
                demo.clicksLeft === 0 && (
                  <Button
                    variant="danger"
                    onClick={() => demo.resetClick()}
                  >
                    reset click
                  </Button>
                )
              }
            </p>
          </Jumbotron>
        </Col>
        <Col xs={12} md={6} lg={4} xl={3}>
          <h4>Async Fetching Demi</h4>
          <Form.Control
            type="text"
            placeholder="http://test.com"
            value={url}
            onChange={(e) => setUrl(e.target.value)}
          />
          <Button variant="danger" onClick={() => demo.fetchAsyncData(url)}>Simulate a asynchronous call</Button>
          <pre>{demo.asyncData ? demo.asyncData : 'No data yet'}</pre>
        </Col>
      </Row>
    </Container>
  );
}

export default observer(WelcomeMobx);
